'use strict'

var fs = require('fs')
var path = require('path')
var wf = require('run-waterfall')
var tape = require('tape')
var parser = require('../')

var LOCKFILE = path.resolve(__dirname, '..', 'yarn.lock')

tape('parsing', function (t) {
  t.plan(2)
  wf([
    function readLockFile (cb) {
      fs.readFile(LOCKFILE, cb)
    },
    function stringify (buffer, cb) {
      cb(null, buffer.toString())
    },
    parser.parse.bind(parser)
  ], function (err, parsed) {
    if (err) return t.fail(err)
    t.ok(typeof parsed === 'object', 'returns object')
    t.ok(
      Object.keys(parsed).some(function hasTape (pkgName) {
        return pkgName.match('tape@')
      }),
      'has tape as dep'
    )
    t.end()
  })
})
