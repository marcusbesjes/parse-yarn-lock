var which = require('which')
var fs = require('fs')
var wf = require('run-waterfall')
var path = require('path')
var isWin = require('os').platform().match(/^win/)
var debug = require('debug')('parse-yarn-lock')

module.exports = {
  parse: function (lockfile, cb) {
    return wf([
      this.getGlobalYarnPath,
      this.getGlobalYarnParser,
      function bindLockfileToParser (parser, cb) {
        return cb(null, parser(lockfile))
      }
    ], cb)
  },
  getGlobalYarnPath: function (cb) {
    return which('yarn', function (err, resolvedPath) {
      if (err) throw err
      debug('yarn path: ' + resolvedPath)
      return fs.realpath(resolvedPath, cb)
    })
  },
  getGlobalYarnParser: function (yarnPath, cb) {
    // <yarn>/bin/yarn ==> (src) <yarn>/src/lockfile
    //                     (compiled) <yar>lib/node_modules/yarn/lib/lockfile/parse.js
    var parseSlug = 'lib/lockfile/parse.js'
    var osSlug = isWin
      ? 'node_modules/yarn'
      : '../..'
    var parserPath = path.resolve(yarnPath, osSlug, parseSlug)
    debug('parser path: ' + parserPath)
    var parser = require(parserPath)
    return cb(null, parser.default)
  }
}
